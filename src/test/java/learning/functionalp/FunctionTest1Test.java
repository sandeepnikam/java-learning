package learning.functionalp;


import learning.pojo.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

public class FunctionTest1Test {

    @Test
    public void FunctionInterfaceTest(){

        List<Person> persons = Arrays.asList(
                new Person("sachin", "tendulkar", 48)
                ,new Person("saurav", "ganguly", 49)
                ,new Person("rahul", "dravid", 33)
        );


        //Function<Person, String> _function1 = person -> person.getFirstName();
        System.out.println("====STYLE ONE===");
        persons.stream().map(person -> person.getFirstName()).forEach(System.out::println);

        //Function<Person, String> _function2 = Person::getFirstName;
        System.out.println("====STYLE TWO===");
        persons.stream().map(Person::getFirstName).forEach(System.out::println);

        Function<Person, String> _function1 = person -> person.getFirstName();
        Function<Person, String> _function2 = Person::getFirstName;

        Consumer<Person> printf0 = person -> System.out.println(person);
        Consumer<Person> printf1 = System.out::println;

        Consumer<Person> printf2 = person -> person.iAmConsumer(person);

        // below case is not working, as it became ambigous


        //Consumer<Person> printf3 = Person::iAmConsumer;

    }


    @Test
    public void functionChaining(){

        Predicate<Person> test1 = person -> person.getFirstName().contains("sac");
        Predicate<Person> test2 = test1.and(person -> person.getLastName().contains("kar"));

        Assertions.assertTrue(test2.test(new Person("sachin", "tendulkar", 45)));
    }
}